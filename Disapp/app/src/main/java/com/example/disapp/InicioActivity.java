package com.example.disapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class InicioActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
    }

    public void registrarse ( View v) {

        Intent intencion = new Intent(this, RegistroActivity.class);
        startActivity(intencion);
    }

    public void iniciarSesion ( View v) {

        Intent intencion = new Intent(this, SesionActivity.class);
        startActivity(intencion);
    }
}
