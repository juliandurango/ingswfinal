package com.example.disapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.Button;

public class LlamadaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_llamada);
        Button mDialButton = (Button) findViewById(R.id.btnllamar);
        Button mDialButton2 = (Button) findViewById(R.id.btnllamar2);

        mDialButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String policia = "123";
                if(!TextUtils.isEmpty(policia)) {
                    String npol = "tel:" + policia;
                    startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(npol)));
                }
            }
        });

        mDialButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String salud = "125";
                if(!TextUtils.isEmpty(salud)) {
                    String nsal = "tel:" + salud;
                    startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(nsal)));
                }
            }
        });
    }

    public void verOpcion (View v){
        Intent intencion = new Intent( this, OpcionActivity.class);
        startActivity(intencion);
    }

}
