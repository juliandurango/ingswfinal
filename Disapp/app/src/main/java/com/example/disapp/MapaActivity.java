package com.example.disapp;

import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapaActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng p1 = new LatLng(6.293598951716228, -75.58519141573218);
        LatLng p2 = new LatLng(6.215040957696338, -75.57526827708739);
        LatLng p3 = new LatLng(6.236536272417958, -75.5922159352456);
        LatLng p4 = new LatLng(6.222912087654082, -75.61175003306792);
        LatLng p5 = new LatLng(6.223402477213384, -75.62891091073007);
        LatLng p6 = new LatLng(6.236950329484975, -75.57045912921903);
        LatLng p7 = new LatLng(6.23698538742998, -75.61928282609244);
        LatLng p8 = new LatLng(6.246612064323503, -75.62547124324152);
        LatLng p9 = new LatLng(6.249274282659759, -75.63186882734584);
        LatLng p10 = new LatLng(6.232834258890915, -75.59551467827737);
        LatLng p11 = new LatLng(6.263313740987232, -75.62788109833656);
        LatLng p12 = new LatLng(6.235551484287633, -75.59648613835563);
        LatLng p13 = new LatLng(6.205261974222128, -75.58865537031359);
        LatLng p14 = new LatLng( 6.284970370986209, -75.58397272991493);
        LatLng p15 = new LatLng(6.297530086084387, -75.53807016720333);
        LatLng p16 = new LatLng(6.249002206604419, -75.54785143449232);
        LatLng p17 = new LatLng(6.243064734828514, -75.5536254984915);
        LatLng p18 = new LatLng(6.201641446690901, -75.59049696810233);
        LatLng p19 = new LatLng(6.2108730889426855, -75.59834390514301);
        LatLng p20 = new LatLng(6.21036189136849, -75.59754038989617);
        LatLng p21 = new LatLng(6.216299930545277, -75.59330380700688);
        LatLng p22 = new LatLng(6.254586417110304, -75.61624926085473);
        LatLng p23 = new LatLng(6.24047770770396, -75.5453013784575);
        LatLng p24 = new LatLng(6.240911007145623, -75.58740547518126);
        LatLng p25 = new LatLng(6.253377694478032, -75.59176001156776);
        LatLng p26 = new LatLng(6.279321723724086, -75.63118685403988);
        LatLng p27 = new LatLng(6.325203687629997, -75.6591703787896);
        LatLng p28 = new LatLng(6.35295980580502, -75.69626743524094);
        LatLng p29 = new LatLng(6.3888034587659615, -75.70048399989098);
        LatLng p30 = new LatLng(6.450958245013192, -75.73556066291317);
        LatLng p31 = new LatLng(6.238814670847181, -75.6009495526517);
        LatLng p32 = new LatLng(6.3165070478443335, -75.6007098353618);
        LatLng p33 = new LatLng(6.227432823808765, -75.52780122549953);
        LatLng p34 = new LatLng(6.183677793143991, -75.46489693328066);
        LatLng p35 = new LatLng(6.25805061555754, -75.60034379597691);
        LatLng p36 = new LatLng(6.2112235835235206, -75.57043309548023);
        LatLng p37 = new LatLng(6.207725881223791, -75.56894730729837);
        LatLng p38 = new LatLng(6.22626462765664, -75.59127174503459);
        LatLng p39 = new LatLng(6.305409847616657, -75.56659198689908);
        LatLng p40 = new LatLng(6.291650604254686, -75.56940514534708);
        LatLng p41 = new LatLng(6.257857712219277, -75.56654074960589);
        LatLng p42 = new LatLng(6.255070288168333, -75.56291515756983);
        LatLng p43 = new LatLng(6.256810606816797, -75.56618501841493);
        LatLng p44 = new LatLng(6.242193756524983, -75.59263712626134);
        LatLng p45 = new LatLng(6.183759652076511, -75.57854735000728);
        LatLng p46 = new LatLng(6.238754400200749, -75.58773744231834);
        LatLng p47 = new LatLng(6.256570326163499, -75.61949926004458);
        LatLng p48 = new LatLng(6.1874678596721, -75.58151116627101);
        LatLng p49 = new LatLng(6.268336814600869, -75.56359387267491);
        LatLng p50 = new LatLng(6.179969623033584, -75.65883688301577);

        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(true);

        mMap.addMarker(new MarkerOptions().position(p1).title(""));
        mMap.addMarker(new MarkerOptions().position(p2).title(""));
        mMap.addMarker(new MarkerOptions().position(p3).title(""));
        mMap.addMarker(new MarkerOptions().position(p4).title(""));
        mMap.addMarker(new MarkerOptions().position(p5).title(""));
        mMap.addMarker(new MarkerOptions().position(p6).title(""));
        mMap.addMarker(new MarkerOptions().position(p7).title(""));
        mMap.addMarker(new MarkerOptions().position(p8).title(""));
        mMap.addMarker(new MarkerOptions().position(p9).title(""));
        mMap.addMarker(new MarkerOptions().position(p10).title(""));
        mMap.addMarker(new MarkerOptions().position(p11).title(""));
        mMap.addMarker(new MarkerOptions().position(p12).title(""));
        mMap.addMarker(new MarkerOptions().position(p13).title(""));
        mMap.addMarker(new MarkerOptions().position(p14).title(""));
        mMap.addMarker(new MarkerOptions().position(p15).title(""));
        mMap.addMarker(new MarkerOptions().position(p16).title(""));
        mMap.addMarker(new MarkerOptions().position(p17).title(""));
        mMap.addMarker(new MarkerOptions().position(p18).title(""));
        mMap.addMarker(new MarkerOptions().position(p19).title(""));
        mMap.addMarker(new MarkerOptions().position(p20).title(""));
        mMap.addMarker(new MarkerOptions().position(p21).title(""));
        mMap.addMarker(new MarkerOptions().position(p22).title(""));
        mMap.addMarker(new MarkerOptions().position(p23).title(""));
        mMap.addMarker(new MarkerOptions().position(p24).title(""));
        mMap.addMarker(new MarkerOptions().position(p25).title(""));
        mMap.addMarker(new MarkerOptions().position(p26).title(""));
        mMap.addMarker(new MarkerOptions().position(p27).title(""));
        mMap.addMarker(new MarkerOptions().position(p28).title(""));
        mMap.addMarker(new MarkerOptions().position(p29).title(""));
        mMap.addMarker(new MarkerOptions().position(p30).title(""));
        mMap.addMarker(new MarkerOptions().position(p31).title(""));
        mMap.addMarker(new MarkerOptions().position(p32).title(""));
        mMap.addMarker(new MarkerOptions().position(p33).title(""));
        mMap.addMarker(new MarkerOptions().position(p34).title(""));
        mMap.addMarker(new MarkerOptions().position(p35).title(""));
        mMap.addMarker(new MarkerOptions().position(p36).title(""));
        mMap.addMarker(new MarkerOptions().position(p37).title(""));
        mMap.addMarker(new MarkerOptions().position(p38).title(""));
        mMap.addMarker(new MarkerOptions().position(p39).title(""));
        mMap.addMarker(new MarkerOptions().position(p40).title(""));
        mMap.addMarker(new MarkerOptions().position(p41).title(""));
        mMap.addMarker(new MarkerOptions().position(p42).title(""));
        mMap.addMarker(new MarkerOptions().position(p43).title(""));
        mMap.addMarker(new MarkerOptions().position(p44).title(""));
        mMap.addMarker(new MarkerOptions().position(p45).title(""));
        mMap.addMarker(new MarkerOptions().position(p46).title(""));
        mMap.addMarker(new MarkerOptions().position(p47).title(""));
        mMap.addMarker(new MarkerOptions().position(p48).title(""));
        mMap.addMarker(new MarkerOptions().position(p49).title(""));
        mMap.addMarker(new MarkerOptions().position(p50).title(""));

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(p3, 18));
    }
}
