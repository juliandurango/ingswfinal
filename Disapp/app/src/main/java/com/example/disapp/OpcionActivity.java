package com.example.disapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class OpcionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opcion);
        Button llamada = (Button) findViewById(R.id.btnllamar);
        final EditText numF = (EditText) findViewById(R.id.numero);

        llamada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String familiar = numF.getText().toString();
                if(!TextUtils.isEmpty(familiar)) {
                    String dial = "tel:" + familiar;
                    startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(dial)));
                }else{
                    Toast.makeText(OpcionActivity.this, "Ingrese número", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void agregarFamiliar ( View v){
        Intent i = new Intent( this, FamiliarActivity.class);
        startActivity(i);
    }
}
