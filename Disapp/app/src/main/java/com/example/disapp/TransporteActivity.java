package com.example.disapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import java.util.ArrayList;
import android.widget.ArrayAdapter;

public class TransporteActivity extends AppCompatActivity {

    private ListView listview;
    private ArrayList<String> rutas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transporte);

        ListView list = (ListView) findViewById(R.id.list);

        rutas = new ArrayList<String>();
        rutas.add("Circular Coonatra 300 - 301");
        rutas.add("Calasanz Boston 310 - 311");
        rutas.add("Santra Belén 315 - 316");
        rutas.add("Ruta de la Salud 308 - 309");
        rutas.add("Laureles 190 - 191 - 192 - 193");
        rutas.add("Picacho 209");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, rutas);
        list.setAdapter(adapter);

    }

    public void elegirRuta ( View v) {
        Intent intencion = new Intent(this, RutaActivity.class);
        startActivity(intencion);
    }
}
