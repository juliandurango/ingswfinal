package com.example.disapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class FamiliarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_familiar);
    }

    public void agregar( View v){
        Intent intencion = new Intent( this, MainActivity.class);
        startActivity(intencion);
    }
}
