package com.example.disapp;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void sendMessage(View view) {
        // Do something in response to button
    }

    public void verMapa ( View v){

        Intent intencion = new Intent( this, MapaActivity.class);
        startActivity(intencion);
    }

    public void definirRuta ( View v) {

        Intent intencion = new Intent(this, RutaActivity.class);
        startActivity(intencion);
    }

    public void verTransporte ( View v) {

        Intent intencion = new Intent(this, TransporteActivity.class);
        startActivity(intencion);
    }

    public void solicitarLlamada ( View v) {

        Intent intencion = new Intent(this, LlamadaActivity.class);
        startActivity(intencion);
    }

}
